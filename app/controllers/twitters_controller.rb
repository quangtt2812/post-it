include TwitterAuth

class TwittersController < ApplicationController
  def index
    @twitter = Twitter.new

    if params['oauth_token'].present? && params['oauth_verifier'].present?
      previous_oauth_token = cookies[:prev_oauth_token]
      previous_oauth_token_secret = cookies[:prev_oauth_token_secret]
      cookies[:prev_oauth_verifier] = params['oauth_verifier']
      if params['oauth_token'] == previous_oauth_token
        access_token(previous_oauth_token, previous_oauth_token_secret, params['oauth_verifier'])
        message = cookies[:status_message]
        last_oauth_token = @oauth_data['oauth_token'].first
        last_oauth_token_secret = @oauth_data['oauth_token_secret'].first
        update_status(last_oauth_token, last_oauth_token_secret, message)
      else
        @error = 'Invalid Token'
      end
    end
  end

  def create
    cookies[:status_message] = params['twitter']['message']

    request_token

    previous_oauth_token = @token_data['oauth_token'].first
    previous_oauth_token_secret = @token_data['oauth_token_secret'].first

    cookies[:prev_oauth_token] = previous_oauth_token
    cookies[:prev_oauth_token_secret] = previous_oauth_token_secret

    redirect_to TWITTER_AUTHENTICATE_URL + previous_oauth_token

  end
end