require 'base64'
require 'uri'
require 'cgi'

require 'net/https'

module TwitterAuth

  OAUTH_CALLBACK = ''
  OAUTH_CONSUMER_KEY = 'ioAbrSp6eEipd9IlTqWaiHwa5'
  OAUTH_CONSUMER_SECRET = 'MonVjMh38d4GMqShTsTgIZoOWO5VJmu4mZYLYzgkH6IJBaqrei'
  OAUTH_SIGNATURE_METHOD = 'HMAC-SHA1'
  OAUTH_VERSION = '1.0'

  TWITTER_REQUEST_TOKEN_URL = 'https://api.twitter.com/oauth/request_token'
  TWITTER_AUTHENTICATE_URL = 'https://api.twitter.com/oauth/authenticate?oauth_token='
  TWITTER_ACCESS_TOKEN_URL = 'https://api.twitter.com/oauth/access_token'
  TWITTER_UPDATE_STATUS_URL = 'https://api.twitter.com/1.1/statuses/update.json'

  METHOD_POST = 'POST'

  @oauth_nonce = ''
  @oauth_timestamp = ''
  @oauth_signature = ''

  def request_token
    timestamp_and_nonce

    data = {
      :oauth_callback => OAUTH_CALLBACK
    }
    parameter_string = build_parameter_string(data)
    signature_base_string = build_signature_base_string(TWITTER_REQUEST_TOKEN_URL, parameter_string)
    signing_key = CGI.escape(OAUTH_CONSUMER_SECRET) + '&'
    hmac = OpenSSL::HMAC.digest(OpenSSL::Digest.new('sha1'), signing_key, signature_base_string)
    signature = Base64.encode64(hmac).chomp.gsub( /\n/, '' )

    # Net::HTTPS
    uri = URI.parse(TWITTER_REQUEST_TOKEN_URL)
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true

    auth_data = {
        :oauth_signature => signature,
    }
    headers = {
        'Authorization' => build_authorization_header(auth_data),
        'Content-Type' => 'application/x-www-form-urlencoded'
    }
    form_data = {
        'oauth_callback' => CGI.escape(OAUTH_CALLBACK)
    }
    request = Net::HTTP::Post.new(uri.request_uri, headers)
    request.set_form_data(form_data)

    response = http.request(request)

    @token_data = CGI::parse(response.body)

  end

  def access_token(oauth_token, oauth_token_secret, oauth_verifier)
    timestamp_and_nonce

    data = {
      :oauth_token => oauth_token,
      :oauth_verifier => oauth_verifier
    }
    parameter_string = build_parameter_string(data)
    signature_base_string = build_signature_base_string(TWITTER_ACCESS_TOKEN_URL, parameter_string)
    signing_key = CGI.escape(OAUTH_CONSUMER_SECRET) + '&' + CGI.escape(oauth_token_secret)
    hmac = OpenSSL::HMAC.digest(OpenSSL::Digest.new('sha1'), signing_key, signature_base_string)
    signature = Base64.encode64(hmac).chomp.gsub( /\n/, '' )

    # Net::HTTPS
    uri = URI.parse(TWITTER_ACCESS_TOKEN_URL)
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true

    auth_data = {
        :oauth_signature => signature,
        :oauth_token => oauth_token
    }
    headers = {
        'Authorization' => build_authorization_header(auth_data),
        'Content-Type' => 'application/x-www-form-urlencoded'
    }
    form_data = {
        'oauth_verifier' => CGI.escape(oauth_verifier)
    }
    request = Net::HTTP::Post.new(uri.request_uri, headers)
    request.set_form_data(form_data)

    response = http.request(request)

    @oauth_data = CGI::parse(response.body)

  end

  def update_status(oauth_token, oauth_token_secret, message)
    timestamp_and_nonce

    data = {
        :oauth_token => oauth_token,
        :status => message
    }
    parameter_string = build_parameter_string(data)
    signature_base_string = build_signature_base_string(TWITTER_UPDATE_STATUS_URL, parameter_string)
    signing_key = CGI.escape(OAUTH_CONSUMER_SECRET) + '&' + CGI.escape(oauth_token_secret)
    hmac = OpenSSL::HMAC.digest(OpenSSL::Digest.new('sha1'), signing_key, signature_base_string)
    signature = Base64.encode64(hmac).chomp.gsub( /\n/, '' )

    # Net::HTTPS
    uri = URI.parse(TWITTER_UPDATE_STATUS_URL)
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true

    auth_data = {
      :oauth_signature => signature,
      :oauth_token => oauth_token
    }
    headers = {
        'Authorization' => build_authorization_header(auth_data),
        'Content-Type' => 'application/x-www-form-urlencoded'
    }
    form_data = {
        'status' => message
    }
    request = Net::HTTP::Post.new(uri.request_uri, headers)
    request.set_form_data(form_data)

    response = http.request(request)

  end

  private

  def timestamp
    Time.now.to_i
  end

  def nonce
    Base64.encode64(@oauth_timestamp.to_s).chomp.gsub( /\n/, '' )
  end

  def timestamp_and_nonce
    @oauth_timestamp = timestamp
    @oauth_nonce = nonce
  end

  def build_parameter_string(input_data)
    parameter_string = ''
    default_data = {
      :oauth_consumer_key     => OAUTH_CONSUMER_KEY,
      :oauth_nonce            => @oauth_nonce,
      :oauth_signature_method => OAUTH_SIGNATURE_METHOD,
      :oauth_timestamp        => @oauth_timestamp,
      :oauth_version          => OAUTH_VERSION
    }
    _data = default_data.merge(input_data)
    data = _data.sort.to_h
    length = data.length

    data.each_with_index do |(key, value), index|
      parameter_string << CGI.escape(key.to_s) << '='

      # Status string needs URI escape
      if key.to_s == 'status'
        parameter_string << URI.escape(value.to_s)
      else
        parameter_string << CGI.escape(value.to_s)
      end

      if index != (length - 1)
        parameter_string << '&'
      end
    end

    parameter_string
  end

  def build_signature_base_string(url, parameter_string)
    signature_base_string = ''
    signature_base_string << METHOD_POST.upcase << '&' << \
    CGI.escape(url) << '&' << \
    CGI.escape(parameter_string)
  end

  def build_authorization_header(input_data)
    authorization_string = 'OAuth '
    default_data = {
        :oauth_consumer_key     => OAUTH_CONSUMER_KEY,
        :oauth_nonce            => @oauth_nonce,
        :oauth_signature_method => OAUTH_SIGNATURE_METHOD,
        :oauth_timestamp        => @oauth_timestamp,
        :oauth_version          => OAUTH_VERSION
    }
    _data = default_data.merge(input_data)
    data = _data.sort.to_h
    length = data.length

    data.each_with_index do |(key, value), index|
      authorization_string << CGI.escape(key.to_s) << '="'
      authorization_string << CGI.escape(value.to_s) << '"'
      if index != (length - 1)
        authorization_string << ', '
      end
    end

    authorization_string
  end
end